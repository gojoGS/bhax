package polargen;

import java.util.Random;

public class PolarGen {

    //Variables
    private boolean hasStoraged = false;
    private double storagedNumber;

    

    public double Next() throws NullPointerException {
        if(!hasStoraged){
            double u1,u2,v1,v2,w;
            Random random = new Random();
            do {
                u1 = random.nextDouble();
                u2 = random.nextDouble();
                v1 = 2 * u1 - 1;
                v2 = 2 * u2 - 1;
                w = v1 * v1 + v2 * v2;
            } while (w > 1);

            double r = Math.sqrt((-2 * Math.log(w))/w );
            
            storagedNumber = r * v2;
            hasStoraged = !hasStoraged;

            return r * v1;
        } else {
            hasStoraged = !hasStoraged;
            return storagedNumber;
        }
    }
}
