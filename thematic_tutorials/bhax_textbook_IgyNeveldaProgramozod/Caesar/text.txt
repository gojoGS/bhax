Hobo Blues Band - Édes otthon

Hányszor hallom, mondják: ó édes otthon,
Erről nagyon hamar le is kellett szoknom.
Anyám a konyhában gyászolta magát,
Kocsmába küldött, hogy hozzam el apát.
Szóltam: Fater, jöjjön haza már,
20 forintot adott, hozzak még piát.

A bátyám melózott, megunta hamar,
Elment, már nem is tudom svéd-e vagy magyar.
Évente hazajön, önmagától részeg,
Visszahívja mindig, ó a családi fészek.
Asztalra borulva egész este sírt,
Vegyek majd neki Kerepesen sírt. ó-ó

Falvédőmön ez áll:
Az édes otthon visszavár,
Visszavár.

Presszó előtt állnak bizonytalan lányok,
Közöttük éhesen, nyugtalanul járok.
Át kellene jutnom még ezen a poklon,
Bableves csülökkel, vár az édes otthon.
Lemegyek a térre, sör és kártya vár,
Zsebemben két ásszal már nem érhet kár (ó nem).
