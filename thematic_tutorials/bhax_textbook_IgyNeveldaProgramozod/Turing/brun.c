#include <stdio.h>

int prime (int number)
{
	int number_of_divisors=0;
	for (int divisor=1; divisor<=number; divisor++)
	{
		if (number%divisor==0)
			{number_of_divisors++;}

	}
	if (number_of_divisors==2)
		{return 1;}
	else{return 0;};

}

int main (void)
{

float sum = 0;

for (int index =1; index <= 10000; index++ )
	{
		if(prime(index)==1&&prime(index+2)==1)
		{
			sum = sum + 1/float(index) + 1/float(index+2);
			printf ("%d and %d -->> %f",index,index+2,sum);

		}
	}
return 0;

}
